################################
##############ZeOS #############
################################
########## Makefile ############
################################

AS86 = as86 -0 -a
LD86 = ld86 -0

HOSTCFLAGS = -Wall -Wstrict-prototypes -g
HOSTCC = gcc
CC = gcc
AS = as --32
LD = ld
OBJCOPY = objcopy -O binary -R .note -R .comment -S

CFLAGS = -m32 -O2 -g -fno-omit-frame-pointer -ffreestanding -fno-stack-protector -fno-PIC -Wall -Iinclude $(if $(RUNTEST), -DRUNTEST)
LDFLAGS = -g -melf_i386 -L lib

OBJDIR = build
LDSDIR = scripts

BOCHS = bochs_nogdb
BOCHSGDB = bochs
GDB = gdb

.PHONY: all clean

all: build zeos.bin

include boot/Makefile
include sys/Makefile
include usr/Makefile
include tools/Makefile

zeos.bin: $(OBJDIR)/bootsect $(OBJDIR)/system $(OBJDIR)/user $(OBJDIR)/build
	$(OBJCOPY) $(OBJDIR)/system $(OBJDIR)/system.out
	$(OBJCOPY) $(OBJDIR)/user $(OBJDIR)/user.out
	$(OBJDIR)/build $(OBJDIR)/bootsect $(OBJDIR)/system.out $(OBJDIR)/user.out > zeos.bin

emul: all
	$(BOCHSGDB) -q -f .bochsrc

gdb: all
	$(BOCHSGDB) -q -f .bochsrc_gdb &
	$(GDB) -x .gdbcmd build/system

emuldbg: all
	$(BOCHS) -q -f .bochsrc

clean:
	rm -rf $(OBJDIR)
	rm -f zeos.bin
	rm -f bochsout.txt

build:
	mkdir -p $(OBJDIR)
	mkdir -p $(OBJDIR)/boot
	mkdir -p $(OBJDIR)/sys
	mkdir -p $(OBJDIR)/usr

