/*
 * io.h - Definició de l'entrada/sortida per pantalla en mode sistema
 */

#ifndef __IO_H__
#define __IO_H__

#include <types.h>

/** Screen functions **/
/**********************/

#define KEYBOARD_DATA_REG 0x60

Byte inb (unsigned short port);
void printc(char c);
void printc_xy(Byte x, Byte y, char c);
void printk(const char *string);
void printk_xy(Byte mx, Byte my, const char *string);
void setScreenColor(Byte color);

#endif  /* __IO_H__ */
