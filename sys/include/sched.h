/*
 * sched.h - Estructures i macros pel tractament de processos
 */

#ifndef __SCHED_H__
#define __SCHED_H__

#include <list.h>
#include <types.h>
#include <mm_address.h>
#include <stats.h>

#define NR_TASKS      10
#define KERNEL_STACK_SIZE	1024
#define DEFAULT_QUANTUM     100;

enum state_t { ST_RUN, ST_READY, ST_BLOCKED };

struct dir_t {
    int used;
    void *sbrk;
};

struct task_struct {
  int PID;			/* Process ID. This MUST be the first field of the struct. */
  page_table_entry * dir_pages_baseAddr;
  unsigned long *kernel_ebp; // This position in the system stack stores ebp from dynamic link in task_switch
  unsigned int quantum;
  struct stats st;
  struct list_head list;
  int rip_sem;
  int readSize;
  struct dir_t *dir;
} *idle_task, *task1_task;

union task_union {
  struct task_struct task;
  unsigned long stack[KERNEL_STACK_SIZE];    /* pila de sistema, per procés */
};

extern union task_union task[NR_TASKS]; /* Vector de tasques */


#define KERNEL_ESP(t)       	(&((union task_union *) t)->stack[KERNEL_STACK_SIZE])

#define INITIAL_ESP       	(DWord) KERNEL_ESP(&task[1])

/* Inicialitza les dades del proces inicial */
void init_task1(void);

void init_idle(void);

void init_sched(void);

void init_stats(struct task_struct *t);

struct task_struct * current();

void task_switch(union task_union*t);

struct task_struct * get_free_task();

void enqueue_free_task(struct task_struct *);

void enqueue_ready_task(struct task_struct *);

struct task_struct *list_head_to_task_struct(struct list_head *l);

page_table_entry * allocate_DIR(struct task_struct *t);

int *usage_DIR(page_table_entry *dir);

page_table_entry * get_PT (struct task_struct *t) ;

page_table_entry * get_DIR (struct task_struct *t) ;

struct task_struct * get_task(int pid);

/* Headers for the scheduling policy */
void sched_next_rr();
void update_process_state_rr(struct task_struct *t, struct list_head *dest);
int needs_sched_rr();
void update_sched_data_rr();
void schedule();
int get_quantum (struct task_struct *t);
void set_quantum (struct task_struct *t, int new_quantum);

extern unsigned int remaining_ticks;
void reset_quantum();

void show_pid();

#endif  /* __SCHED_H__ */
