/*
 * sem.h - Estructures i macros per la sincronització de processos
 */

#ifndef __SEM_H__
#define __SEM_H__

#include <list.h>

#define NR_SEMS 20

struct sem_t {
    int id;
    int owner;
    int value;
    int initialized;
    struct list_head list;   
};

extern struct sem_t semaphores[NR_SEMS];

void init_semaphores();

struct sem_t *getSemaphore(int id);

struct sem_t *getFreeSemaphore();

#endif //__SEM_H__
