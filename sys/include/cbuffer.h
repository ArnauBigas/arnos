#ifndef CBUFFER_H
#define CBUFFER_H

typedef struct {
    unsigned char *data;
    int head;
    int tail;
    int size;
    int capacity;
} cbuffer_t;

void cbuffer_push(cbuffer_t *b, unsigned char val);

unsigned char cbuffer_pop(cbuffer_t *b);

int cbuffer_full(cbuffer_t *b);

#endif
