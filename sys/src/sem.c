#include <sem.h>
#include <types.h>

struct sem_t semaphores[NR_SEMS];

void init_semaphores()
{
    for(int i = 0; i < NR_SEMS; i++)
        semaphores[i].initialized = 0;
}

struct sem_t *getSemaphore(int id)
{
    for(int i = 0; i < NR_SEMS; i++)
        if(semaphores[i].id == id) return &semaphores[i];
    return NULL;
}

struct sem_t *getFreeSemaphore()
{
    for(int i = 0; i < NR_SEMS; i++)
        if(semaphores[i].initialized == 0) return &semaphores[i];
    return NULL;
}
