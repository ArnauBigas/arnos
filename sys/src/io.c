/*
 * io.c - 
 */

#include <io.h>

#include <types.h>

/**************/
/** Screen  ***/
/**************/

#define NUM_COLUMNS 80
#define NUM_ROWS    25

Byte x, y=19;
Byte color = 0x02;

/* Read a byte from 'port' */
Byte inb (unsigned short port)
{
  Byte v;

  __asm__ __volatile__ ("inb %w1,%0":"=a" (v):"Nd" (port));
  return v;
}

void wordcpy(Word *dst, Word *src, int size) {
    while(size-- > 0)
        *dst++ = *src++;
}

void wordset(Word *dst, Word val, int size) {
    while(size-- > 0)
        *dst++ = val;
}

Word *screen = (Word *)0xb8000;

void newline() {
    x = 0;
    if (y == NUM_ROWS - 1) {
        wordcpy(screen, screen + NUM_COLUMNS, NUM_COLUMNS * (NUM_ROWS-1));
        wordset(screen + ((NUM_ROWS-1)*NUM_COLUMNS), (color & 0xF0) << 4 | 0x20, NUM_COLUMNS);
    } else {
        y++;
    }
}

void printc(char c)
{
    __asm__ __volatile__ ( "movb %0, %%al; outb $0xe9" ::"a"(c)); /* Magic BOCHS debug: writes 'c' to port 0xe9 */
    if (c=='\n') {
        newline();
    } else {
        Word ch = (Word) (c & 0x00FF) | (color << 8);
        screen[(y * NUM_COLUMNS + x)] = ch;
        if (++x >= NUM_COLUMNS) newline();
    }
}

void printc_xy(Byte mx, Byte my, char c)
{
  Byte cx, cy;
  cx=x;
  cy=y;
  x=mx;
  y=my;
  printc(c);
  x=cx;
  y=cy;
}

void printk(const char *string)
{
  int i;
  for (i = 0; string[i]; i++)
    printc(string[i]);
}

void printk_xy(Byte mx, Byte my, const char *string)
{
  int i;
  for (i = 0; string[i]; i++)
    printc_xy(mx + i, my, string[i]);
}

void setScreenColor(Byte c)
{
    color = c;
}
