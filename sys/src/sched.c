/*
 * sched.c - initializes struct for task 0 anda task 1
 */

#include <sched.h>
#include <mm.h>
#include <io.h>
#include <utils.h>

union task_union task[NR_TASKS]
  __attribute__((__section__(".data.task")));

#if 1
struct task_struct *list_head_to_task_struct(struct list_head *l)
{
  return list_entry( l, struct task_struct, list);
}
#endif

extern struct list_head blocked;
struct list_head freequeue;
struct list_head readyqueue;

struct task_struct *idle_task;

struct dir_t dirs[NR_TASKS];

/* get_DIR - Returns the Page Directory address for task 't' */
page_table_entry * get_DIR (struct task_struct *t) 
{
	return t->dir_pages_baseAddr;
}

/* get_PT - Returns the Page Table address for task 't' */
page_table_entry * get_PT (struct task_struct *t) 
{
	return (page_table_entry *)(((unsigned int)(t->dir_pages_baseAddr->bits.pbase_addr))<<12);
}


page_table_entry * allocate_DIR(struct task_struct *t)
{
    for (int i = 0; i < NR_TASKS; ++i)
        if (dirs[i].used == 0) {
            dirs[i].used = 1;
            dirs[i].sbrk = (void *) L_HEAP_START;
            t->dir = &dirs[i];
            return &dir_pages[i][ENTRY_DIR_PAGES];
        }
    printk("\nNo free directories.\n");
    print_dir_stats();
    return 0;
}

void cpu_idle(void)
{
	__asm__ __volatile__("sti": : :"memory");

	while(1)
	{
	}
}

void init_stats(struct task_struct *t)
{
    t->st.user_ticks = 0;
    t->st.system_ticks = 0;
    t->st.blocked_ticks = 0;
    t->st.ready_ticks = 0;
    t->st.elapsed_total_ticks = get_ticks();
    t->st.total_trans = 0;
    t->st.remaining_ticks = t->quantum;
}
void init_idle (void)
{
    // Get the first freequeue entry
    struct list_head *idle_head = list_first(&freequeue);
    // Delete it from the queue
    list_del(idle_head);
    // idle_task must NOT be in readyqueue
    // Pointer conversion
    idle_task = list_head_to_task_struct(idle_head);
    // Initialize PID and page directory of this task
    idle_task->PID = 0;
    idle_task->dir_pages_baseAddr = allocate_DIR(idle_task);
    // Pointer conversion
    union task_union *idle_union = (union task_union *) idle_task;
    // Execution context
    KERNEL_ESP(idle_union)[-1] = (DWord) &cpu_idle; // Return address at the bottom of the stack.
    KERNEL_ESP(idle_union)[-2] = 0; // Fake ebp content (will be pop'd but not used).
    idle_task->kernel_ebp = &KERNEL_ESP(idle_union)[-2];
    idle_task->quantum = DEFAULT_QUANTUM;
    init_stats(idle_task);
}

void set_task_pages(union task_union * const t)
{
    // Setting the system stack in the TSS and the MSR required by sysenter.
    // These locations had been previously initialized with INITIAL_ESP.
    tss.esp0 = (DWord) KERNEL_ESP(t);
    writeMsr(0x175, KERNEL_ESP(t));
    // Set the  current  page  directory.
    if (t->task.dir_pages_baseAddr != current()->dir_pages_baseAddr)
        set_cr3(t->task.dir_pages_baseAddr);
}

void init_task1(void)
{
    struct list_head *task1_head = list_first(&freequeue);
    list_del(task1_head);
    task1_task = list_head_to_task_struct(task1_head);
    task1_task->PID = 1;
    task1_task->dir_pages_baseAddr = allocate_DIR(task1_task);
    set_user_pages(task1_task);
    union task_union *task1_union = (union task_union *) task1_task;
    // I'm not sure about this:
    task1_task->kernel_ebp = 0;
    set_task_pages(task1_union);
    task1_task->quantum = DEFAULT_QUANTUM;
    remaining_ticks = DEFAULT_QUANTUM;
    init_stats(task1_task);
}


void init_sched()
{
    INIT_LIST_HEAD(&freequeue);
    INIT_LIST_HEAD(&readyqueue);
    
    for(int i = 0; i < NR_TASKS; i++) {
        list_add(&(task[i].task.list), &freequeue);
        dirs[i].used = 0;
        task[i].task.PID = -1;
    }
}

struct task_struct* current()
{
  int ret_value;
  
  __asm__ __volatile__(
  	"movl %%esp, %0"
	: "=g" (ret_value)
  );
  return (struct task_struct*)(ret_value&0xfffff000);
}

struct task_struct * get_free_task()
{
    if (!list_empty(&freequeue)) {
        struct list_head *head = list_first(&freequeue);
        list_del(head);
        return list_head_to_task_struct(head);
    } else return NULL;
}

void enqueue_free_task(struct task_struct *task)
{
    list_add(&task->list, &freequeue);
}

void enqueue_ready_task(struct task_struct *task)
{
    list_add_tail(&task->list, &readyqueue);
}

void __show_pid(const char*, int);

void show_pid()
{
    __show_pid("Running process: ", current()->PID);
}

/////////////////////////////////////////////////////////////////////////
/* SCHEDULING */

// Global variable to control the number of ticks the current process has been
// running since last task_switch. It shall be decreased every tick.

unsigned int remaining_ticks = 0;

int get_quantum (struct task_struct *t)
{
    return t->quantum;
}

void set_quantum (struct task_struct *t, int new_quantum)
{
    t->quantum = new_quantum;
}

int needs_sched_rr()
{
    // When remaining_ticks is zero and readyqueue is empty, an "optimized"
    // context switch takes place. Transitions count must be updated in this case too.
    return remaining_ticks == 0 && (!list_empty(&readyqueue) ? 1 : (current()->st.total_trans++, 0));
}

void update_process_state_rr(struct task_struct *t, struct list_head *dest)
{
    struct list_head *t_list = &t->list;
    if (!not_in_list(t_list)) // if current state is not 'running'
        list_del(t_list);
    if (dest) // if next state is not 'running'
        list_add_tail(t_list, dest);
}

void sched_next_rr()
{
    if(list_empty(&readyqueue)) {
        task_switch((union task_union *)idle_task);
    } else {
        struct list_head *head = list_first(&readyqueue);
        list_del(head);
        struct task_struct *task = list_head_to_task_struct(head);
        task_switch((union task_union *) task);
    }
}

void schedule()
{
    --remaining_ticks;
    if (needs_sched_rr()) {
        struct task_struct *current_task = current();
        if (current_task != idle_task)
            update_process_state_rr(current_task, &readyqueue);
        sched_next_rr();
    } else if (remaining_ticks == 0)
        reset_quantum();
}

void reset_quantum() {
    remaining_ticks = current()->quantum;
}

/////////////////////////////////////////////////////////////////////////
/* STATISTICS */

struct task_struct * get_task(int pid)
{
    for(int i = 0; i < NR_TASKS; i++) {
        struct task_struct *task_struct = &(task[i].task);
        if (task_struct->PID == pid) return task_struct;
    }
    return 0;
}

void usr2sys()
{
    struct stats *st = &current()->st;
    st->user_ticks += get_ticks() - st->elapsed_total_ticks;
    st->elapsed_total_ticks = get_ticks();
    st->remaining_ticks = remaining_ticks;
}

void sys2usr()
{
    struct stats *st = &current()->st;
    st->system_ticks += get_ticks() - st->elapsed_total_ticks;
    st->elapsed_total_ticks = get_ticks();
    st->remaining_ticks = remaining_ticks;
}

void sys2ready()
{
    struct stats *st = &current()->st;
    st->system_ticks += get_ticks() - st->elapsed_total_ticks;
    st->elapsed_total_ticks = get_ticks();
    st->remaining_ticks = remaining_ticks;
}

void ready2sys()
{
    struct stats *st = &current()->st;
    st->ready_ticks += get_ticks() - st->elapsed_total_ticks;
    st->elapsed_total_ticks = get_ticks();
    st->remaining_ticks = remaining_ticks;
    st->total_trans++;
}

//////////////////////////////////////////////////////////////////////

// Ok, this should not be here, we'll fix this when we have more time.

void sys_itoa(int a, char *b)
{
  int i, i1;
  char c;

  if (a==0) { b[0]='0'; b[1]=0; return ;}

  i=0;
  if (a < 0)
  {
    b[0] = '-';
    a = -a;
    ++b;
  }
  while (a>0)
  {
    b[i]=(a%10)+'0';
    a=a/10;
    i++;
  }

  for (i1=0; i1<i/2; i1++)
  {
    c=b[i1];
    b[i1]=b[i-i1-1];
    b[i-i1-1]=c;
  }
  b[i]=0;
}

int sys_strlen(const char *a)
{
  int i;

  i=0;

  while (a[i]!=0) i++;

  return i;
}

/////////////////////////////////////////////////////////////////////////

void __show_pid(const char* msg, int pid)
{
    printk_xy(0, 3, msg);
    char buf[32] = {0};
    sys_itoa(pid, buf);
    printk_xy(sys_strlen(msg), 3, buf);
    for (int x = sys_strlen(msg)+sys_strlen(buf); x < 80; ++x)
        printc_xy(x, 3, ' ');
    printc_xy(80, 3, '\n');
}

void print_dir_stats() {
    printk("\n!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!\n");
    for (int i = 0; i < NR_TASKS; ++i) {
        char buf[32] = {0};
        sys_itoa(task[i].task.PID, buf);
        printk("Task: ");
        printk(buf);
        sys_itoa(((unsigned int)(task[i].task.dir_pages_baseAddr) - (unsigned int)(dir_pages))/(sizeof(page_table_entry)*TOTAL_PAGES), buf);
        printk(" -- Using directory: ");
        printk(buf);
        printk("\n");
    }
    for (int i = 0; i < NR_TASKS; ++i) {
        char buf[32] = {0};
        sys_itoa(i, buf);
        printk("Directory: ");
        printk(buf);
        sys_itoa(dirs[i].used, buf);
        printk(" -- Used by: ");
        printk(buf);
        printk("\n");
    }
    printk("!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!\n");
}
