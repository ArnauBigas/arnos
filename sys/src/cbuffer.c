#include <cbuffer.h>

void cbuffer_push(cbuffer_t *b, unsigned char val) {
    b->data[b->head++] = val;
    if(b->head == b->capacity) b->head = 0;
    b->size++;
}

unsigned char cbuffer_pop(cbuffer_t *b) {
    unsigned char val = b->data[b->tail++];
    if (b->tail == b->capacity) b->tail = 0;
    b->size--;
    return val;
}

int cbuffer_full(cbuffer_t *b) {
    return b->size == b->capacity;
}
