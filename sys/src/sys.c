/*
 * sys.c - Syscalls implementation
 */
#include <devices.h>

#include <utils.h>

#include <io.h>

#include <mm.h>

#include <mm_address.h>

#include <sched.h>

#include <interrupt.h>
#include <errno.h>
#include <stats.h>
#include <sem.h>

#define LECTURA 0
#define ESCRIPTURA 1

int check_fd(int fd, int permissions)
{
  if (fd!=1) return -EBADF;
  if (permissions!=ESCRIPTURA) return -EACCES;
  return 0;
}

int sys_ni_syscall()
{
	return -38; /*ENOSYS*/
}

int sys_getpid()
{
	return current()->PID;
}

int ret_from_fork()
{
    return 0;
}

static int lastPID = 1;

int sys_fork()
{
    // creates the child process
    // a) get a free task_struct
    struct task_struct *task = get_free_task();
    if(task == NULL) return -EAGAIN;

    // b) inherit system data
    copy_data(current(), task, sizeof(union task_union));

    // c) initialize field with new directory
    task->dir_pages_baseAddr = allocate_DIR(task);
    page_table_entry *childPTE = get_PT(task);
    task->dir->sbrk = current()->dir->sbrk;

    // d) search physical pages in which to map the new data + stack
    int NUM_PAG_DATAHEAP = NUM_PAG_DATA + NUM_PAG_HEAP(task->dir->sbrk);
    int frames[NUM_PAG_DATAHEAP];
    for (int i = 0; i < NUM_PAG_DATAHEAP; i++) {
        int frame = alloc_frame();
        if(frame < 0) {
            for(int j = 0; j < i; j++) free_frame(frames[j]);
            enqueue_free_task(task);
            return -ENOMEM;
        }
        frames[i] = frame;
    }

    // e) inherit user data
    page_table_entry *parentPTE = get_PT(current());
    //    i) create new address space
    for(int i = 1; i < NUM_PAG_KERNEL; i++) //Null convention
        set_ss_pag(childPTE, i, get_frame(parentPTE, i));

    for(int i = 0; i < NUM_PAG_CODE; i++)
        set_ss_pag(childPTE, i+PAG_LOG_INIT_CODE, get_frame(parentPTE, i+PAG_LOG_INIT_CODE));

    for(int i = 0; i < NUM_PAG_DATAHEAP; i++)
        set_ss_pag(childPTE, i+PAG_LOG_INIT_DATA, frames[i]);

    //    ii) copy user data+stack
    for(int i = 0; i < NUM_PAG_DATAHEAP; i++)
        set_ss_pag(parentPTE, i+PAG_LOG_INIT_DATA+NUM_PAG_DATAHEAP, frames[i]);

    copy_data((DWord*) (PAG_LOG_INIT_DATA*PAGE_SIZE),
              (DWord*) ((PAG_LOG_INIT_DATA+NUM_PAG_DATAHEAP)*PAGE_SIZE),
              PAGE_SIZE*NUM_PAG_DATAHEAP);

    for(int i = 0; i < NUM_PAG_DATAHEAP; i++)
        del_ss_pag(parentPTE, i+PAG_LOG_INIT_DATA+NUM_PAG_DATAHEAP);

    set_cr3(get_DIR(current()));

    // f) assign new PID
    task->PID = ++lastPID;

    // g) initialize different task_struct fields
    init_stats(task);

    // h) prepare child stack for task_switch
	// ebp value in this function is KERNEL_BASE_ESP-0x11 beucase of SAVE_ALL+5 registers saved
	KERNEL_ESP(task)[-0x12] = (DWord) ret_from_fork;
	KERNEL_ESP(task)[-0x13] = 0xDEADBEEF;
	task->kernel_ebp = &KERNEL_ESP(task)[-0x13];

    // i) insert into readyqueue
    enqueue_ready_task(task);

    // j) return child PID
    return task->PID;
}

int sys_clone(void (*function)(void), void *stack)
{
    if(!access_ok(VERIFY_READ, function, 1)) return -EFAULT;
    if(!access_ok(VERIFY_WRITE, stack, 1)) return -EFAULT;
    // creates the child process
    // a) get a free task_struct
    struct task_struct *task = get_free_task();
    if(task == NULL) return -EAGAIN;

    // b) inherit system data
    copy_data(current(), task, sizeof(union task_union));

    //Directory has one more usage
    task->dir->used++;

    // f) assign new PID
    task->PID = ++lastPID;

    // g) initialize different task_struct fields
    init_stats(task);

    // h) prepare child stack for task_switch
	// ebp value in this function is KERNEL_BASE_ESP-0x11 beucase of SAVE_ALL+5 registers saved
	KERNEL_ESP(task)[-0x12] = (DWord) ret_from_fork;
	KERNEL_ESP(task)[-0x13] = 0xDEADBEEF;
	task->kernel_ebp = &KERNEL_ESP(task)[-0x13];
    
    //Change user stack and instruction pointer
    KERNEL_ESP(task)[-2] = (DWord) stack;
    KERNEL_ESP(task)[-5] = (DWord) function;

    // i) insert into readyqueue
    enqueue_ready_task(task);

    // j) return child PID
    return task->PID;
}

void sys_exit()
{
    struct task_struct *task = current();
    int NUM_PAG_DATAHEAP = NUM_PAG_DATA + NUM_PAG_HEAP(task->dir->sbrk);
    if (--task->dir->used == 0) {
        page_table_entry *PT = get_PT(task);
        for(int i = 0; i < NUM_PAG_DATAHEAP; i++) {
            free_frame(get_frame(PT, i+PAG_LOG_INIT_DATA));
            del_ss_pag(PT, i+PAG_LOG_INIT_DATA);
        }
    }

    for (int i = 0; i < NR_SEMS; i++)
        if(semaphores[i].owner == task->PID)
            sys_sem_destroy(semaphores[i].id);

    enqueue_free_task(task);
    task->PID = -1;
    sched_next_rr();
}

#define SYS_WRITE_BUF_SIZE 512

int sys_write(int fd, const char* buffer, int size)
{
    int ret = check_fd(fd, ESCRIPTURA);
    if (ret != 0) return ret;

    if (buffer == NULL) return -EFAULT;

    if(size < 0) return -EINVAL;

    char buf[SYS_WRITE_BUF_SIZE];
    int copied = 0;
    while ((size - copied) > 0) {
        int seg = min(SYS_WRITE_BUF_SIZE, size - copied);
        if(copy_from_user(buffer + copied, buf, seg) != 0) return -3;
        sys_write_console(buf, seg);
        copied += seg;
    }

    return copied;
}

int sys_read(int fd, const char *buffer, int size)
{
    if (buffer == NULL) return -EFAULT;
    if (!access_ok(VERIFY_WRITE, buffer, size)) return -EFAULT;
    if (fd < 0) return -EBADF;

    current()->readSize = size;
    if (fd == 0) return sys_read_keyboard(buffer);
    else return -EBADF;
}

int sys_gettime()
{
    return zeos_ticks;
}

int sys_get_stats(int pid, struct stats *st)
{
    if(!access_ok(VERIFY_WRITE, st, sizeof(struct stats))) return -EFAULT;
    if(pid < 0) return -EINVAL;
    struct task_struct *task = get_task(pid);
    if (task != 0) {
        struct stats * task_st = &(task->st);
        task_st->remaining_ticks = remaining_ticks;
        copy_to_user(task_st, st, sizeof(struct stats));
    }
    return task == 0 ? -ESRCH : 0;
}

int sys_sem_init(int n_sem, unsigned int value) {
    struct sem_t *sem = getSemaphore(n_sem);
    if(sem != NULL && sem->initialized) return -EBUSY;

    sem = getFreeSemaphore();
    if(sem == NULL) return -EINVAL;

    INIT_LIST_HEAD(&sem->list);
    sem->id = n_sem;
    sem->owner = current()->PID;
    sem->initialized = 1;
    sem->value = value;

    return 0;
}

int sys_sem_wait(int n_sem) {
    struct sem_t *sem = getSemaphore(n_sem);
    if (sem == NULL || !sem->initialized) return -EINVAL;

    if(--sem->value < 0) {
        update_process_state_rr(current(), &sem->list);
        sched_next_rr();
    }
    return !current()->rip_sem ? 0 : (current()->rip_sem = 0, -EINVAL);
}

int sys_sem_signal(int n_sem) {
    struct sem_t *sem = getSemaphore(n_sem);
    if (sem == NULL || !sem->initialized) return -EINVAL;

    sem->value++;
    if (!list_empty(&sem->list)) {
        struct list_head *head = list_first(&sem->list);
        list_del(head);
        enqueue_ready_task(list_head_to_task_struct(head));
    }

    return 0;
}

int sys_sem_destroy(int n_sem) {
    struct sem_t *sem = getSemaphore(n_sem);
    if (sem == NULL) return -EINVAL;
    if (!sem->initialized) return -EPERM;
    if(sem->owner != current()->PID) return -EPERM;

    int retval = 0;
    sem->initialized = 0;
    while(!list_empty(&sem->list)) {
        retval = 1;
        struct list_head *head = list_first(&sem->list);
        list_del(head);
        struct task_struct *task = list_head_to_task_struct(head);
        task->rip_sem = 1;
        enqueue_ready_task(task);
    }
    return retval;
}

void *sys_sbrk(int increment) {
    struct task_struct *t = current();
    void *brk = t->dir->sbrk;

    t->dir->sbrk += increment;
    if(t->dir->sbrk < (void *) L_HEAP_START) t->dir->sbrk = L_HEAP_START;

    page_table_entry *pt = get_PT(current());
    int pages = NUM_PAG_HEAP(t->dir->sbrk) - NUM_PAG_HEAP(brk);

    if (pages > 0) {
        // ¡UN VLA! ¡A CUBIERTO!
        int frames[pages];
        for (int i = 0; i < pages; i++) {
            int frame = alloc_frame();
            if(frame < 0) {
                for(int j = 0; j < i; j++) free_frame(frames[j]);
                t->dir->sbrk = brk;
                return (void *) -ENOMEM;
            }
            frames[i] = frame;
        }
        for(int i = 0; i < pages; i++)
            set_ss_pag(pt, i+(L_HEAP_START/PAGE_SIZE)+NUM_PAG_HEAP(brk), frames[i]);
    } else if (pages < 0) {
        for(int i = 0; i < -pages; i++) {
            free_frame(get_frame(pt, i+(L_HEAP_START/PAGE_SIZE)+NUM_PAG_HEAP(t->dir->sbrk)));
            del_ss_pag(pt, i+(L_HEAP_START/PAGE_SIZE)+NUM_PAG_HEAP(t->dir->sbrk));
        }

        set_cr3(get_DIR(t));
    }

    return brk;
}
