#include <libc.h>

char buff[24];

int pid;

void printstr(char *str) {
     write(1, str, strlen(str));
}

int __attribute__ ((__section__(".text.main")))
  main(void)
{
#ifdef RUNTEST
    runjp();
    exit();
#endif
    printstr("Hello, World!\n");
    int *b = sbrk(0);
    int *p = sbrk(sizeof(int));
    *p = 69;
    int pid = fork();
    *p = 42;
    while(1) {}
}


